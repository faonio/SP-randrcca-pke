from charm.toolbox.pairinggroup import PairingGroup,ZR,G1,G2,GT,pair,pc_element
import numpy as np
from collections import namedtuple

PK=namedtuple('PK',['D','E','a_Trasl_D','f_Trasl_D','F_Trasl_D','g_Trasl_E','G_Trasl_E','GDstar','FE'])
SK=namedtuple('SK',['a','f','g','F','G','genG1','genG2'])

class InputError(Exception):
  pass
  
class RandRCCA:
 
 def __init__(self,k,group):
   self.k=k
   self.group=group
 
 def init1(self,k,n): #for vector inizialization, all elements are initialized to 1
 
   #Initialize a vector of dimension k with element 1 of group G1/G2/GT
 
   #Args: 
   #     k = Vector dimension; 
   #     n = Parameter used to decide between G1, G2, GT (n=1 to have elements of group   G1, n=2 to have elements of group G2, n=3 to have elements of group GT,
 
   #Returns: 
   #     Vector of dimension k with element = 1 belonging to the selected group 
   #     Null if the parameter n doesn't refer to G1, G2, GT
   v = np.empty([k],dtype=pc_element)
   for i in range(k):
    if n==1: 			#if elements belong to G1
     v[i] = group.init(G1,1) 
    elif n==2: 			#if elements belong to G2
     v[i] = group.init(G2,1) 
    elif n==3: 			#if elements belong to GT
     v[i] = group.init(GT,1) 
    else:
     return null
   return v


 def ComputeMatrixGroup(self,k1,k2,M,genG): #for pk matrices computation in enc
   #Compute genG**M[i][j] for each element of M
 
   #Initialize matrices of the pk
 
   #Args: 
   #     k1,k2 = Matrix dimensions; 
   #     M = Matrix with elements of group ZR used as exponent of the fixed element of group G1/G2/GT
   #     genG = fixed element of group G1/G2/GT
 
   #Returns: 
   #     Matrix with element of group G1/G2/GT  
 
   Matrix = np.empty([k1,k2],dtype=pc_element)
   for i in range (k1):
     for j in range (k2):
      Matrix[i][j] = genG ** M[i][j] 
   return Matrix


 def ComputeVectorGroup(self,k1,v,genG): #for pk vectors computation in enc
   #Compute genG**v[i] for each element of v
 
   #Initialize vector of the pk
 
   #Args: 
   #     k1 = Vector dimensions; 
   #     v = Vector with element of group ZR used as exponent of the fixed element of group G1/G2/GT
   #     genG = fixed element of group G1/G2/GT
 
   #Returns: 
   #     Vector with element of group G1/G2/GT
  
   Vector = np.empty([k1],dtype=pc_element)
   for i in range (k1):
     Vector[i] = genG ** v[i]
   return Vector

 def ExpMatrixVector(self,k1,k2,v,M,v1): #for exponentiation between matrix and vector
   #Compute M**v1
 
   #Compute exponentiation between a matrix M containing elements of group G1/G2/GT and vector v1 containing ZR elements
 
   #Args: 
   #     k1,k2 = Matrix dimensions, k2 is also the dimension of the vector v1; 
   #     v = vector of dimension k1 in which is stored the result of this exponentiation 
   #     M = Matrix with elements of group G1/G2/GT
   #     v1 = vector with elements of group ZR 
 
   #Returns: 
   #     Vector v result of this exponentiation containing elements of group G1/G2/GT (same group of elements of matrix M)
 
   for i in range(k1):
    for j in range(k2):
     v[i] *= M[i][j] ** v1[j]
   return v


 def ExpVectorMatrix(self,k1,k2,v,M,v1): #for exponentiation between vector and matrix
   #Compute v1**M
 
   #Compute exponentiation between a vector v1 containing elements of group G1/G2/GT and a matrix M containing ZR elements
 
   #Args: 
   #     k1,k2 = Matrix dimensions, k2 is also the dimension of the vector v1 
   #     v = vector of dimension k1 in which is stored the result of this exponentiation 
   #     M = Matrix with elements of group ZR
   #     v1 = vector with elements of group G1/G2/GT 
 
   #Returns: 
   #     Vector v result of this exponentiation containing element of group G1/G2/GT (same group of element of vector v)
 
   for i in range(k1):
    for j in range(k2):
     v[i] *= v1[j] ** M[i][j]
   return v
 
 def ExpVectorVector(self,k1,v,v1,v2): #for exponentiation between two vectors 
   #Compute v1**v2
 
   #Compute exponentiation between a vector v1 containing elements of group G1/G2/GT and a vector v2 containing ZR elements
 
   #Args: 
   #     k1 = Vectors dimension 
   #     v1 = vector with elements of group G1/G2/GT
   #     v2 = vector with elements of group ZR
   #     v =  value in which is stored the result of this exponentiation
 
   #Returns: 
   #     Value v in which is stored the result of this exponentiation, containing element of group G1/G2/GT (same group of element of vector v1)
 
   for i in range(k1):
     v *= v1[i] ** v2[i]
   return v
  
 def pairing(self,k,v1,v2): #compute pairing between two vectors
   pairing=group.init(GT,1)
   for i in range(k):
    pairing *= pair(v1[i],v2[i]) 
   return pairing

 
 #define all generators
 def matrix_generator(self,n): #generator of matrices
  #Generate random matrices
   
   #Args: 
   #     self.k = base value decided in the implementation 
   #     group = selected group
   #     n = parameter used to decide which type of matrices should be sampled
 
   #Returns: 
   #     Random D matrix having chosen dimensions
    k=self.k
    while True:
     if n==0: #return random [(k+1)*(k)] matrix
      dim1 = k+1
      dim2 = k
     elif n==1: #return random [(k+1)*(k+1)] matrix
      dim1 = k+1
      dim2 = k+1
     elif n==2: #return random [(k+1)*(k+2)] matrix
      dim1 = k+1
      dim2 = k+2
     D = np.empty([dim1,dim2],dtype=pc_element)
     for i in range (dim1):
      for j in range (dim2):
       D[i][j] = self.group.random(ZR)
     yield D

 def vector_generator(self,n): #generator of vectors
   #Generate random vectors
 
   #Args: 
   #     k = base value decided in the implementation 
   #     group = selected group
   #     n = parameter used to decide which type of vector should be sampled
 
   #Returns: 
   #     Random a vector having chosen dimensions 
   k=self.k 
   while True: 
     if n==0: #return random [k+1] dimension vector
      dim = k+1
     elif n==1: #return random [k] dimension vector
      dim = k
     a = np.empty(dim,dtype=pc_element)
     for i in range (dim):
      a[i] = self.group.random(ZR)
     a=a.T
     yield a

 
 def KGen(self):  
   #sample all vectors and matrices
   k=self.k
   group=self.group
   mymatrixgenerator = self.matrix_generator(0)
   D = next(mymatrixgenerator)
   E = next(mymatrixgenerator)
   mymatrixgenerator = self.matrix_generator(1)
   F = next(mymatrixgenerator)
   mymatrixgenerator = self.matrix_generator(2)
   G = next(mymatrixgenerator)
   myvectorgenerator = self.vector_generator(0)
   a = next(myvectorgenerator)
   f = next(myvectorgenerator)
   g = next(myvectorgenerator)
 
   #set D* (add a line to D to obtain D1)
   temp = np.dot(a.T,D)
   D1 = np.vstack([D,temp])
 
   # generate G1,G2,GT
   genG1=group.random(G1)
   genG2=group.random(G2)
   genGT=pair(genG1,genG2)
 
   #set sk
   sk = SK(a=a,f=f,g=g,F=F,G=G,genG1=genG1,genG2=genG2)
 
   #compute pk
   Dpk = self.ComputeMatrixGroup(k+1,k,D,genG1) 
 
   Epk = self.ComputeMatrixGroup(k+1,k,E,genG2)
 
   temp = np.dot(a.T,D)
   a_Trasl_D = self.ComputeVectorGroup(k,temp,genG1)
 
   temp = np.dot(f.T,D)
   f_Trasl_D = self.ComputeVectorGroup(k,temp,genGT) 
 
   temp = np.dot(F.T,D)
   F_Trasl_D = self.ComputeMatrixGroup(k+1,k,temp,genG1) 
 
   temp = np.dot(g.T,E)
   g_Trasl_E = self.ComputeVectorGroup(k,temp,genGT)  
 
   temp = np.dot(G.T,E)
   G_Trasl_E = self.ComputeMatrixGroup(k+2,k,temp,genG2) 
 
   temp = np.dot(G,D1)
   GDstar = self.ComputeMatrixGroup(k+1,k,temp,genG1)
 
   temp = np.dot(F,E)
   FE = self.ComputeMatrixGroup(k+1,k,temp,genG2) 
 
   #set pk
   pk = PK(D=Dpk,E=Epk,a_Trasl_D=a_Trasl_D,f_Trasl_D=f_Trasl_D,F_Trasl_D=F_Trasl_D,g_Trasl_E=g_Trasl_E,G_Trasl_E=G_Trasl_E,GDstar=GDstar,FE=FE)
   return (sk,pk)

 def Enc(self,pk,M):
 
   k=self.k
   group=self.group 
   #sample r,s
   myvectorgenerator = self.vector_generator(1)
   r = next(myvectorgenerator)
   s = next(myvectorgenerator)
 
   #compute u
   D = pk.D
   u = self.init1(k+1,1)
   u = self.ExpMatrixVector(k+1,k,u,D,r) #compute D*r
   u=u.T 
 
   #compute p
   p=group.init(G1,1)
   a_Trasl_D = pk.a_Trasl_D
   p = self.ExpVectorVector(k,p,a_Trasl_D,r) #compute a_Trasl_D*r
   p = p * M
 
   #compute v
   E = pk.E 
   v = self.init1(k+1,2)
   v = self.ExpMatrixVector(k+1,k,v,E,s) #compute E*s
   v=v.T 
 
   #compute x
   x = np.empty([k+2],dtype=pc_element)
   for i in range(k+2):
    if i == k+1:
     x[i] = p
    else :
     x[i] = u[i]
   x=x.T 
 
   #compute pi greco 1
 
   #compute first factor of pi_greco1 (fattore1)
   f_Trasl_D = pk.f_Trasl_D 
   fattore1 = group.init(GT,1)
   fattore1 = self.ExpVectorVector(k,fattore1,f_Trasl_D,r) #compute f_Trasl_D*r
 
   #compute second factor (pairing) of pi_greco1 (pairing1)
   F_Trasl_D = pk.F_Trasl_D
   vett1 = self.init1(k+1,1)
   vett1 = self.ExpMatrixVector(k+1,k,vett1,F_Trasl_D,r) #compute first element of the first pairing: F_Trasl_D*r
   pairing1 = self.pairing(k+1,vett1,v) #compute pairing between F_Trasl_D*r and v
 
   pi_greco1 = fattore1 * pairing1
 
   #compute pi greco 2
 
   #compute first factor of pi_greco2 (fattore2)
   g_Trasl_E=pk.g_Trasl_E
   fattore2 = group.init(GT,1)
   fattore2 = self.ExpVectorVector(k,fattore2,g_Trasl_E,s) #compute g_Trasl_E*s

   #compute second factor of pi_greco2 (pairing2)
   G_Trasl_E = pk.G_Trasl_E
   vett2 = self.init1(k+2,2)
   vett2 = self.ExpMatrixVector(k+2,k,vett2,G_Trasl_E,s) #compute second element of second pairing: G_Trasl_E*s
   pairing2 = self.pairing(k+2,x,vett2) #compute pairing between x and G_Trasl_E*s
 
   pi_greco2 = fattore2 * pairing2
 
   #compute pi_greco
   pi_greco = pi_greco1 * pi_greco2
 
   C=(x,v,pi_greco)
   return C
 
 
 def Dec(self,sk,C):
 
   #extract x,v,pi_greco from C
   x = C[0]
   v = C[1]
   pi_greco_iniziale = C[2]
   xT=x.T
   k = (len(xT)-2)
 
   #extract u and p
   u = np.empty([k+1],dtype=pc_element)
   for i in range(k+2):
    if i == (k+1):
     p = xT[i]
    else :
     u[i] = xT[i]
 
   #compute M
   a = sk.a
   aT=a.T
   temp = group.init(G1,1)
   temp = self.ExpVectorVector(k+1,temp,u,aT) #compute u*aT
   M = p / temp #decode the initial message
 
 
   #compute pi_greco1
 
   #compute first factor of pi_greco1 (pairing1)
   F=sk.F
   vett = self.init1(k+1,2)
   vett = self.ExpVectorMatrix(k+1,k+1,vett,F,v) #compute F*v
   pairing1 = self.pairing(k+1,u,vett)# pairing between Fv e u
 
   #compute second factor of pi_greco1 (fattore1)
   uTarget = self.init1(k+1,3)
   genG2 = sk.genG2
   for i in range(k+1):
    uTarget[i] = pair(u[i],genG2) # pairing between U and genG2 to compute uTarget
   f=sk.f
   f=f.T
   fattore1 = group.init(GT,1)
   fattore1 = self.ExpVectorVector(k+1,fattore1,uTarget,f) #compute uTarget*f
 
   pi_greco1 = pairing1 * fattore1
 
   #compute pi_greco2
 
   #compute first factor of pi_greco2 (pairing2)
   G=sk.G
   vett1 = self.init1(k+1,1)
   vett1 = self.ExpVectorMatrix(k+1,k+2,vett1,G,x) #compute G*x
   pairing2 = self.pairing(k+1,vett1,v) #pairing between Gx e v
 
   #compute second factor of pi_greco2 (fattore2)
   vTarget = self.init1(k+1,3)
   genG1 = sk.genG1
   for i in range(k+1):
    vTarget[i] = pair(genG1,v[i]) # pairing between genG1 e v to compute vTarget
   g=sk.g
   g=g.T
   fattore2 = group.init(GT,1)
   fattore2 = self.ExpVectorVector(k+1,fattore2,vTarget,g) #compute vTarget*g
 
   pi_greco2 = pairing2 * fattore2
 
   #compute pi_greco
 
   pi_greco = pi_greco1 * pi_greco2
   if (pi_greco == pi_greco_iniziale): #check if pi_greco are equals
     return M    #if yes return the message
   else :
     error_msg = f"Errore Parametri"  
     raise InputError(error_msg)      #if they are not equals raise an error
   
 def Rand(self,pk,C):
 
   #extract x,v,pi_greco from C
   x = C[0]
   v = C[1]
   pi_greco_iniziale = C[2]
 
   #compute u and p
   xT=x.T
   k = (len(xT)-2)
   u = np.empty([k+1],dtype=pc_element)
   for i in range(k+2):
    if i == (k+1):
     p = xT[i]
    else :
     u[i] = xT[i]
 
   #sample r1 e s1
   myvectorgenerator = self.vector_generator(1)
   r1 = next(myvectorgenerator)
   s1 = next(myvectorgenerator)
 
   #compute D1
   D=pk.D
   a_Trasl_D = pk.a_Trasl_D
   D1 = np.vstack([D,a_Trasl_D]) #add a_Trasl_D as last row of D to obtain D1
 
   #compute x1
   interm = self.init1(k+2,1)
   interm = self.ExpMatrixVector(k+2,k,interm,D1,r1) #compute D1*r1
   
   x1 = np.empty([k+2],dtype=pc_element) 
   for i in range(k+2):
    x1[i] = x[i] * interm[i]
 
   #compute v1
   E=pk.E
   interm2 = self.init1(k+1,2)
   interm2 = self.ExpMatrixVector(k+1,k,interm2,E,s1) #compute E*s1
 
   v1 = np.empty([k+1],dtype=pc_element) 
   for i in range(k+1):
    v1[i] = v[i] * interm2[i]

   #compute pi_greco1
 
   #first factor of pi_greco1 (fattore1)
   f_Trasl_D = pk.f_Trasl_D 
   fattore1 = group.init(GT,1)
   fattore1 = self.ExpVectorVector(k,fattore1,f_Trasl_D,r1)  #compute f_Trasl_D*r1
 
   #second factor of pi_greco1 (pairing1)
   F_Trasl_D = pk.F_Trasl_D
   vett1 = self.init1(k+1,1)
   vett1 = self.ExpMatrixVector(k+1,k,vett1,F_Trasl_D,r1) #compute first element of the first pairing: F_Trasl_D*r1
   pairing1 = self.pairing(k+1,vett1,v1) 	#compute pairing between F_Trasl_D*r1 and v1
 
   #third factor of pi_greco1 (pairing2)
   FE=pk.FE
   vett2 = self.init1(k+1,2)
   vett2 = self.ExpMatrixVector(k+1,k,vett2,FE,s1) 	#compute second element of the second pairing: FE*s1
   pairing2 = self.pairing(k+1,u,vett2)  #compute pairing between u and FE*s1
 
   pi_greco1 = fattore1 * pairing1 * pairing2
 
   #compute pi_greco2
 
   #first factor of pi_greco2 (fattore2)
   g_Trasl_E = pk.g_Trasl_E 
   fattore2 = group.init(GT,1)
   fattore2 = self.ExpVectorVector(k,fattore2,g_Trasl_E,s1)  #compute g_Trasl_E*s1
 
   #second factor of pi_greco2 (pairing3)
   G_Trasl_E = pk.G_Trasl_E
   vett3 = self.init1(k+2,2)
   vett3 = self.ExpMatrixVector(k+2,k,vett3,G_Trasl_E,s1) #compute second element of the third pairing: G_Trasl_E*s1
   pairing3 = self.pairing(k+2,x1,vett3) 	#compute pairing between x1 and G_Trasl_E*s1
 
   #third factor of pi_greco2 (pairing4)
   GDstar=pk.GDstar
   vett4 = self.init1(k+1,1)
   vett4 = self.ExpMatrixVector(k+1,k,vett4,GDstar,r1) #compute first element of the fourth pairing: GDstar*r1
   pairing4 = self.pairing(k+1,vett4,v) 	#compute pairing between GDstar*r1 and v
 
   pi_greco2 = fattore2 * pairing3 * pairing4
 
   #compute pi_greco_tot
   pi_greco_tot = pi_greco_iniziale * pi_greco1 * pi_greco2
 
   C1=(x1,v1,pi_greco_tot)
   return C1

k=2
group=PairingGroup('MNT159')   
rand=RandRCCA(2,group)
sk,pk=rand.KGen() 	# generate sk,pk
M=group.random(G1) 	# generate message
print("M=",M) 	        # print initial message
C=rand.Enc(pk,M) 	#encrypt message
print("C=",C[2]) 	#print initial cyphertext
C1=rand.Rand(pk,C) 	        #randomize cyphertext
print("C1=",C1[2]) 	#print new randomized cyphertext, that should be different from initial cyphertext 
MDec=rand.Dec(sk,C1) 	#decrypt the message using randomized cyphertext
print("M=",MDec) 	#print decrypted message, that should be equal to initial message
